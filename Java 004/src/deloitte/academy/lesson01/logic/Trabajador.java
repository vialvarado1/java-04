package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.Abstractos;

/**
 * 
 * @author vialvarado
 *
 */

public class Trabajador extends Abstractos {
	/**
	 * Esta clase genera los descuentos para trabajadores
	 */
	@Override
	public double cobrar() {
		/**
		 * total es el total del descuento al 10%
		 */

		double total = this.getImporte() * .90;
		return total;
	}

	/**
	 * El metodo trabajador tiene dos parametros uno id Venta u otro de importe
	 */
	public Trabajador() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param idVenta: este parametro es para identificar el tipo de venta
	 * @param importe: este es el costo de la habitacion
	 */
	public Trabajador(int idVenta, double importe) {
		super(idVenta, importe);
		// TODO Auto-generated constructor stub
	}

}
