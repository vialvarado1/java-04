package deloitte.academy.lesson01.logic;
/**
 * 
 * @author Victor Alvarado
 *
 */
public enum Enums {
	
	OK("Tipo de Huesped:  Normal",0),OK2("Tipo de Huesped:  Frecuente",10),OK3("Tipo de Huesped: Trabajador",25);
	
	
public String descripcion;
public int num;
/**
 * 
 * @param descripcion es la descripcion de nuestro enum
 * @param num es el numero del porcentaje de descuento
 */


private Enums(String descripcion, int num) {
	this.descripcion = descripcion;
	this.num = num;
	/**
	 * Se generan getters and setters
	 */
}

public String getDescripcion() {
	return descripcion;
}

public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}

public int getNum() {
	return num;
}

public void setNum(int num) {
	this.num = num;
}



}