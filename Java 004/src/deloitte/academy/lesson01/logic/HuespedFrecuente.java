package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.Abstractos;

/**
 * 
 * @author vialvarado
 *
 */
public class HuespedFrecuente extends Abstractos {

	@Override
	public double cobrar() {
		/**
		 * Este programa genera el descuento de los huspedes frecuentes
		 */
		// TODO Auto-generated method stub

		/*
		 * El descuento es del 25 %
		 * 
		 */
		double total = this.getImporte() * .75;
		return total;
	}

	/**
	 * Despues los envia al run
	 */
	public HuespedFrecuente() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param idVenta guarda el ide de la venta y es de tipo entero
	 * @param importe guarda el costo
	 */
	public HuespedFrecuente(int idVenta, double importe) {
		super(idVenta, importe);
		/**
		 * retorna el descuento
		 */
	}

}
