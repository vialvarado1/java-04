package deloitte.academy.lesson01.entity;

public abstract class Abstractos {
//Sirve para ayudar a otras clases para funcionar pero en si no hace nada por si misma
	
	private int idVenta;
	private double importe;

	public Abstractos() {
		// TODO Auto-generated constructor stub
	
	}
	
	/**
	 * 
	 * @param idVenta es el ID de cada tipo de 
	 * @param importe es
	 */

	public Abstractos(int idVenta, double importe) {
		super();
		this.idVenta = idVenta;
		this.importe = importe;
	}



	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}
	public abstract  double cobrar();
	
}
