package deloitte.academy.lesson01.run;
import deloitte.academy.lesson01.logic.Producto;
import deloitte.academy.lesson01.logic.RegistroNuevosProductos;
import deloitte.academy.lesson01.logic.Enums;
import java.util.ArrayList;
import java.util.logging.Logger;

import deloitte.academy.lesson01.logic.Huesped;
import deloitte.academy.lesson01.logic.HuespedFrecuente;
import deloitte.academy.lesson01.logic.Trabajador;

/**
 * 
 * @author vialvarado
 *
 */

public class Run {
	
	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Este es el programa principal, trae y hereda de otras clases todos los datos
		 */
		
		/*
		 * Aqui se ponen las habitaciones disponibles como objeto
		 */
		
		ArrayList<Producto> listProductoLacteos1 = new ArrayList<Producto>();
		Producto habitacion = new Producto();
		Producto habitacion2 = new Producto();
		Producto habitacion3 = new Producto();
		Producto habitacion4 = new Producto();
		Producto habitacion5 = new Producto();
		Producto habitacion6 = new Producto();
		/**
		 * Aqui se ponen los tres diferentes precios
		 */
		
		Huesped huesped = new Huesped(1, 100);
		huesped.setIdVenta(1);
		huesped.setImporte(100);
		Trabajador trabajador = new Trabajador(2, 300);
		trabajador.setIdVenta(2);
		trabajador.setImporte(330);
		HuespedFrecuente huespedFrecuente = new HuespedFrecuente(4, 400);
		huespedFrecuente.setIdVenta(2);
		huespedFrecuente.setImporte(300);
	
		/*
		 * Aqui se registran
		 */
		
		habitacion.setNombre("Victor Eduardo");
		listProductoLacteos1.add(habitacion);
		System.out.println("Nombre del Huesped:   " + habitacion.getNombre());
		System.out.println("Descuento: " + Enums.OK.num + "%" + "   " + Enums.OK.descripcion);
		System.out.println("Total a pagar: " + huesped.cobrar());
		
		System.out.println(" ");
		
		habitacion2.setNombre("Jenny Rubio");
		listProductoLacteos1.add(habitacion2);
		System.out.println("Nombre del Huesped:   " + habitacion2.getNombre());
		System.out.println("Descuento: " + Enums.OK2.num + "%" + "   " + Enums.OK2.descripcion);
		System.out.println("Total a pagar: " + trabajador.cobrar());
		
		System.out.println(" ");
		
		habitacion3.setNombre("Mario Ivan");
		listProductoLacteos1.add(habitacion3);
		System.out.println("Nombre del Huesped:   " + habitacion3.getNombre());
		System.out.println("Descuento: " + Enums.OK3.num + "%" + "   " + Enums.OK3.descripcion);
		System.out.println("Total a pagar: " + huespedFrecuente.cobrar());
		
		System.out.println(" ");
		
	    habitacion4.setNombre("Eden Gonzales");
		listProductoLacteos1.add(habitacion4);
		System.out.println("Nombre del Huesped:   " + habitacion4.getNombre());
		System.out.println("Descuento: " + Enums.OK.num + "%" + "   " + Enums.OK.descripcion);
		System.out.println("Total a pagar: " + huesped.cobrar());
		
		System.out.println(" ");
		
		habitacion5.setNombre("Javier Gutierrez");
		listProductoLacteos1.add(habitacion5);
		System.out.println("Nombre del Huesped:   " + habitacion5.getNombre());
		System.out.println("Descuento: " + Enums.OK.num + "%" + "   " + Enums.OK.descripcion);
		System.out.println("Total a pagar: " + trabajador.cobrar());
		
		System.out.println(" ");
		
		habitacion6.setNombre("Mauricio");
		listProductoLacteos1.add(habitacion6);
		System.out.println("Nombre del Huesped:   " + habitacion6.getNombre());
		System.out.println("Descuento: " + Enums.OK2.num + "%" + "   " + Enums.OK2.descripcion);
		System.out.println("Total a pagar: " + huespedFrecuente.cobrar());
		
		System.out.println(" ");
		
		int CantidadDeProductoLacVendido1 = 10;
        int total2 = (CantidadDeProductoLacVendido1 - (RegistroNuevosProductos.totalProducto(listProductoLacteos1)));
		RegistroNuevosProductos registroNuevosProductos = new RegistroNuevosProductos();
		listProductoLacteos1.add(registroNuevosProductos.registrar(habitacion));
		
		
		System.out.println("Numero de habitaciones disponibles  " + total2 + "   " + "Habitaciones en Total: "
				+ CantidadDeProductoLacVendido1);
		LOGGER.info("Hotel");
		/**
		 * } public static void ejecutarPago(Abstractos abstractos) {
		 * System.out.println("Total a pagar: "+ abstractos.cobrar()); }
		 */
	}
}
